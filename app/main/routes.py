from app.forms import LoginForm, RegistrationForm
from flask import render_template, flash, redirect, url_for, jsonify
from flask_login import current_user, login_user
from app.models import User, Post, Item, Donate
from flask_login import logout_user
from flask_login import login_required
from app import db
from flask import request
from werkzeug.urls import url_parse
from datetime import datetime
from . import bp as app

@app.route('/')
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    if current_user.user_type == 'admin':
        return redirect(url_for('main.admin'))
    posts = []
    post_objs = Post.query.all()
    user_type = current_user.user_type

    if current_user.user_type == 'R':
        for post in post_objs:
            if post.user_id == current_user.id:
                posts.append(post)
        return render_template("index.html", title='Home Page', posts=posts, user_type= \
            user_type)
    else:
        for post in post_objs:
            posts.append(post)
        donor_objs = Donate.query.filter_by(donor_id = current_user.id)
        donations = []
        for don in donor_objs:
            if don.donor_id == current_user.id:
                print("MATT")
                donations.append(don)
        return render_template("index.html", title='Home Page', posts=posts, donations = donations, user_type= \
            user_type)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        if current_user.user_type == 'admin':
            return redirect(url_for('main.admin'))
        return redirect(url_for('main.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('main.login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('main.index')
            if user.user_type == 'admin':
                next_page = url_for('main.admin')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@app.route('/admin', methods=['GET', 'POST'])
@login_required
def admin():
    if current_user.user_type == 'admin':
        posts = []
        user_posts = []
        post_objs = Post.query.all()
        print(len(post_objs))

        for post in post_objs:
            if User.query.get(post.user_id).username == current_user.username:
                user_posts.append(post)
            posts.append(post)

        donor_objs = Donate.query.all()
        user_donations = []
        for don in donor_objs:
            if don.donor_id == current_user.id:
                print("MATT")
                user_donations.append(don)

        if request.form.get('search', '') != '':
            print(type(request.form.get('search', '')))
            print(type(request.form.get('filter_by', '')))
            search = request.form.get('search', '')

            if request.form.get('filter_by', '') == 'Posts':
                post_objs = Post.query.all()
                posts = []
                for post in post_objs:
                    if search in post.title:
                        posts.append(post)
                    elif search in post.body:
                        posts.append(post)
                    elif search in User.query.get(post.user_id).username:
                        posts.append(post)

                return render_template("admin.html", title='Home Page', posts=posts, user_posts = user_posts, user_donations = user_donations)
            elif request.form.get('filter_by', '') == 'Items':
                item_objs = Item.query.all()
                print(item_objs)
                items = []
                where = {}
                for item in item_objs:
                    if search in item.name:
                        items.append(item)
                    elif search in item.unit:
                        items.append(item)
                print(items)
                return render_template("admin.html", title='Home Page', items=items, user_posts = user_posts, user_donations = user_donations)

            elif request.form.get('filter_by', '') == 'Donations':
                don_objects = Donate.query.all()
                donations = []
                for d in don_objects:
                    try:
                        itemid = d.item_id
                        item = Item.query.get(itemid)
                        if search in item.name:
                            donations.append(d)
                        elif search in item.unit:
                            donations.append(d)
                    except AttributeError:
                        author = 'None'
                print(donations)
                return render_template("admin.html", title='Home Page', donations=donations, user_posts = user_posts, user_donations = user_donations)

        return render_template("admin.html", title='Home Page', posts=posts, user_posts = user_posts, user_donations = user_donations)
    else:
        return redirect(url_for('main.login'))
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data, user_type=form.user_type.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('main.login'))
    return render_template('register.html', title='Register', form=form)

@app.route('/_delete_post')#this can be done only by admin
def delete_post():
    post_id = request.args.get('post_id', 0, type=int)
    post = Post.query.get(post_id)
    if current_user.user_type != 'D':
        db.session.delete(post)
        db.session.commit()
        donates = Donate.query.filter_by(post_id=post_id)
        for i in donates:
            db.session.delete(i)
            db.session.commit()
        items = Item.query.filter_by(post_id = post_id)
        for j in items:
            db.session.delete(i)
            db.session.commit()

    print ('delete_post ', post_id)
    return jsonify(result='post deleted')

@app.route('/_create_post')#this can be done only by admin
def create_post():
    disaster_title = request.args.get('disaster_title')
    disaster_description = request.args.get('disaster_description')
    print ('title = ', disaster_title)
    print ('disaster_description = ', disaster_description)
    p = Post(title=disaster_title, body=disaster_description, author=current_user)

    db.session.add(p)
    db.session.commit()

    post = {'id': p.id, 'title': disaster_title, 'body': disaster_description,
            'author': current_user.username, 'timestamp': p.timestamp}
    user_type = current_user.user_type
    if len(p.title) == 0 or len(p.body)==0 or current_user.user_type == 'D':
        db.session.delete(p)
        db.session.commit()
    # I = Item(post_id=p.id,author=current_user)
    # db.session.add(I)
    # db.session.commit()

    #db.session.delete(post)
    #db.session.commit()
    return jsonify(post=post)

@app.route('/post<int:post_id>',methods=['GET', 'POST'])
def post(post_id):
    p = Post.query.filter_by(id=post_id).first()
    print(p)
    i = Item.query.filter(Item.post_id==post_id).all()
    author = User.query.get(p.user_id).username
    #for i in i:
    #    print(i.post_id, i.quantity, i.name)
    u = User.query.all()
    dic_item = {}
    for item in i:
        dic_item[item.id] = item.name
    dic_user = {}
    for item in u:
        dic_user[item.id] = item.username
    donates = Donate.query.filter_by(post_id=post_id).all()
    print(len(donates))
    for item in donates:
        item.item_id = dic_item[item.item_id]
        item.donor_id = dic_user[item.donor_id]
    #donate = (donor,item_name,quantity,timestap)

    return render_template('post.html',post=p,items=i,donates = donates, author=author)
    #return jsonify(post=p)

@app.route('/_create_item')#this can be done only by admin
def create_item():
    item_name = request.args.get('item_name')
    item_quantity = request.args.get('item_quantity')
    item_unit = request.args.get('item_unit')
    post_id = request.args.get('post_id')
    print ('name = ', item_name)
    print ('item_quantity = ', item_quantity)
    #u = User.query.get(1)
    #p = Post(title=disaster_title, body=disaster_description, author=current_user)
    #db.session.add(p)
    #db.session.commit()
    I = Item(post_id = post_id, name=item_name,quantity=item_quantity,unit = item_unit)
    print(I.post_id)
    print(I.name)
    db.session.add(I)
    db.session.commit()
    #print(111111111)
   # post = {'id':p.id, 'title':disaster_title, 'body':disaster_description,
   # 'author':current_user.username, 'timestamp':p.timestamp}
    #db.session.delete(post)
    #db.session.commit()
    item = {'post_id': post_id,'name':item_name,'quantity':item_quantity,'item_id':I.id,'unit':I.unit}
    if len(I.name) == 0 or I.quantity<=0 or len(I.unit) == 0 or current_user.user_type == 'D':
        db.session.delete(I)
        db.session.commit()
    return jsonify(Item = item)


@app.route('/_delete_item')#this can be done only by admin
def delete_item():
    item_id = request.args.get('item_id', 0, type=int)
    item = Item.query.get(item_id)
    # donations = Donate.query.filter_by(item_id = item_id).all()
    # for d in donations:
    #     d.user_id = 'None'
    if current_user.user_type != 'D':
        db.session.delete(item)
        db.session.commit()
        donations = Donate.query.filter_by(item_id=item_id)
        for donation in donations:
            db.session.delete(donation)
            db.session.commit()
    print ('delete_post ', item_id)
    return jsonify(result='item deleted')

@app.route('/_donate_item',methods=['GET', 'POST'])
def donate_item():
    item_id = request.args.get('item_id',0,type=int)
    donate_quantity = request.args.get('quantity',0,type=int)
    donor_id = request.args.get('donor_id',0,type=int)
    item = Item.query.get(item_id)
    post_id = item.post_id
    if current_user.user_type != 'R':
        item.update_quantity(donate_quantity)

    db.session.commit()

    donate = Donate(post_id = post_id,item_id = item_id, donor_id = donor_id, quantity = donate_quantity)
    db.session.add(donate)
    #print(donate)

    db.session.commit()

    if current_user.user_type=='R':
        db.session.delete(donate)
        db.session.commit()
   # print(donate.post_id, donate.item_id, donate.quantity,donate.id)

    donates = Donate.query.filter_by(donor_id=donor_id).all()
    item_name = Item.query.filter_by(id=item_id).first().name
    donor_name = User.query.filter_by(id=donor_id).first().username
    d = {'post_id':post_id,'donor_name':donor_name,'item_id':item_id,'donor_id':donor_id,'quantity':donate_quantity,'item_name':item_name,'id':donor_id}
    #print(new_quantity)
    print(donates)
    return jsonify(objects='item updated',donate=d)

@app.route('/_delete_donation')#this can be done only by admin
def delete_donation():
    donation_id = request.args.get('item_id', 0, type=int)
    d = Donate.query.get(donation_id)
    # donations = Donate.query.filter_by(item_id = item_id).all()
    # for d in donations:
    #     d.user_id = 'None'
    if current_user.user_type == 'admin':
        db.session.delete(d)
        db.session.commit()
    print ('delete_donate ', d)
    return jsonify(result='item deleted')