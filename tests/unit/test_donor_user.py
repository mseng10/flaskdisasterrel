def test_donor(donor_user):
    assert donor_user.username == 'donor_user'
    assert donor_user.email == 'donor@uiowa.edu'
    assert donor_user.user_type == 'D'
    assert donor_user.check_password('donor')

def test_donor_change_password(donor_user):
    assert donor_user.check_password('donor')

    donor_user.set_password('new_password')

    assert donor_user.check_password('new_password')
    assert not donor_user.check_password('donor')

#TODO Donations for donor user
def test_donor_item():
    assert True