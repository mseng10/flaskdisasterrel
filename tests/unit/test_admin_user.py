def test_admin(admin_user):
    assert admin_user.username == 'admin_user'
    assert admin_user.email == 'admin@uiowa.edu'
    assert admin_user.user_type == 'admin'
    assert admin_user.check_password('admin')

def test_admin_post(admin_post):
    assert admin_post.title == 'Admin Post'
    assert admin_post.body == 'This is a test post for a admin user.'
    assert admin_post.user_id == None

def test_admin_change_password(admin_user):
    assert admin_user.check_password('admin')

    admin_user.set_password('new_password')

    assert admin_user.check_password('new_password')
    assert not admin_user.check_password('admin')

#TODO Admin Item
def test_admin_item(admin_item):
    assert admin_item == None
