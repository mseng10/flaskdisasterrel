from app.config import Config
from tests.conftest import TestConfig

def test_tester_config():
    config = TestConfig()
    assert config.SECRET_KEY == 'team_2_disaster_rel_test'
    assert config.SQLALCHEMY_TRACK_MODIFICATIONS == False
    assert config.SQLALCHEMY_DATABASE_URI != 'sqlite:///'
    assert config.TESTING == True
    assert config.WTF_CSRF_ENABLED == False

def test_config():
    config = Config()
    assert config.SECRET_KEY == 'team_2_disaster_rel'
    assert config.SQLALCHEMY_DATABASE_URI != 'sqlite:///'
    assert config.SQLALCHEMY_TRACK_MODIFICATIONS == False



