from app.models import User, Post, Item

def test_full_db(test_client, test_full_database):
    users = User.query.all()
    posts = Post.query.all()
    items = Item.query.all()

    assert len(users) == 3
    assert len(posts) == 2
    assert len(items) == 1

    user = User.query.filter_by(user_type='R').all()
    assert len(user) == 1
    user = User.query.filter_by(user_type='R').first()
    assert len(user.posts.all()) == 1
    post = user.posts.first()
    assert len(post.Items.all()) == 1

    user = User.query.filter_by(user_type='D').all()
    assert len(user) == 1

    user = User.query.filter_by(user_type='admin').all()
    assert len(user) == 1
    user = User.query.filter_by(user_type='admin').first()
    assert len(user.posts.all()) == 1

def test_full_db_add_user(test_client, test_full_database):
    new_user = User(username='new_user', email='new@uiowa.edu', user_type='D')

    test_full_database.session.add(new_user)
    test_full_database.session.commit()

    assert new_user.is_authenticated == True
    assert new_user.is_active == True

    users = User.query.all()
    assert len(users) == 4

def test_full_db_remove_user(test_client, test_full_database):
    user = User.query.first()
    test_full_database.session.delete(user)
    test_full_database.session.commit()

    users = User.query.all()
    assert len(users) == 2

def test_full_db_add_post(test_client, test_full_database):
    user = User.query.first()
    new_post = Post(title='Test Post',body='Test Post for test db',author=user)

    test_full_database.session.add(new_post)
    test_full_database.session.commit()

    user = User.query.first()
    assert len(user.posts.all()) == 2

def test_full_db_remove_post(test_client, test_full_database):
    post = Post.query.first()
    test_full_database.session.delete(post)
    test_full_database.session.commit()

    user = User.query.first()
    assert len(user.posts.all()) == 0

def test_full_db_add_item(test_client, test_full_database):
    user = User.query.filter_by(user_type='R').first()
    post = user.posts.first()

    new_item = Item(name='new item', quantity=5, unit = 'ms', post = post)
    test_full_database.session.add(new_item)
    test_full_database.session.commit()

    user = User.query.filter_by(user_type='R').first()
    post = user.posts.first()
    assert len(post.Items.all()) == 2

def test_full_db_remove_item(test_client,test_full_database):
    item = Item.query.first()
    test_full_database.session.delete(item)
    test_full_database.session.commit()

    user = User.query.first()
    post = user.posts.first()
    assert len(post.Items.all()) == 0

