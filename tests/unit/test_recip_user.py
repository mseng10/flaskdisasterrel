def test_recip(recip_user):
    assert recip_user.username == 'recip_user'
    assert recip_user.email == 'recip@uiowa.edu'
    assert recip_user.user_type == 'R'
    assert recip_user.check_password('recip')

def test_recip_post(recip_post):
    assert recip_post.title == 'Recip Post'
    assert recip_post.body == 'This is a test post for recip user.'
    assert recip_post.user_id == None

def test_recip_change_password(recip_user):
    assert recip_user.check_password('recip')

    recip_user.set_password('new_password')

    assert recip_user.check_password('new_password')
    assert not recip_user.check_password('recip')

def test_recip_item(recip_item):
    assert recip_item.name == 'recip item'
    assert recip_item.quantity == 10
    assert recip_item.unit == 'units'
