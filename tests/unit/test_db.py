from app.models import User, Post

def test_db(test_client, test_database):
    users = User.query.all()
    posts = Post.query.all()
    assert len(users) == 1
    assert len(posts) == 1

    user = User.query.first()
    assert len(user.posts.all()) == 1

def test_db_add_user(test_client, test_database):
    new_user = User(username='new_user', email='new@uiowa.edu', user_type='D')

    test_database.session.add(new_user)
    test_database.session.commit()

    assert new_user.is_authenticated == True
    assert new_user.is_active == True

    users = User.query.all()
    assert len(users) == 2

def test_db_remove_user(test_client, test_database):
    user = User.query.first()
    test_database.session.delete(user)
    test_database.session.commit()

    user = User.query.all()
    assert len(user) == 0

def test_db_add_post(test_client, test_database):
    user = User.query.first()
    new_post = Post(title='Test Post',body='Test Post for test db',author=user)

    test_database.session.add(new_post)
    test_database.session.commit()

    user = User.query.first()

    assert len(user.posts.all()) == 2

def test_db_remove_post(test_client, test_database):
    post = Post.query.first()
    test_database.session.delete(post)
    test_database.session.commit()

    user = User.query.first()
    assert len(user.posts.all()) == 0