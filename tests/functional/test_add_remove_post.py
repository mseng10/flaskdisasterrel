from app.models import Post, User

def test_admin_remove_post(test_client,test_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data

    posts = Post.query.first().id
    assert posts == 1

    response = test_client.get('/_delete_post',query_string=dict(post_id=posts),
                               follow_redirects=True)

    assert response.status_code == 200
    assert 'Admin Post' not in response.data
    assert len(Post.query.all()) == 0

def test_recip_remove_post(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='recip_user', password='recip', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data

    post = Post.query.filter_by(user_id=3).first()

    response = test_client.get('/_delete_post',query_string=dict(post_id=post.id),
                               follow_redirects=True)

    assert response.status_code == 200
    assert 'Recip Post' not in response.data
    assert len(Post.query.all()) == 1

def test_invalid_remove_post(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data

    posts = Post.query.first().id
    assert posts == 1

    response = test_client.get('/_delete_post', query_string=dict(post_id=posts),
                               follow_redirects=True)

    assert response.status_code == 200
    assert len(Post.query.all()) == 2


def test_admin_add_post(test_client, test_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert len(Post.query.all()) == 1

    response = test_client.get('/_create_post', query_string=dict(disaster_title='new post!',
                                                          disaster_description='body for new post!'
                                                          ),
                               follow_redirects=True)

    assert response.status_code == 200
    assert len(Post.query.all()) == 2
    assert 'new post!' in response.data

def test_donor_no_add_post(test_client, test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data
    assert 'Admin Post' in response.data
    assert len(Post.query.all()) == 2

    response = test_client.get('/_create_post', query_string=dict(disaster_title='new post!',
                                                          disaster_description='body for new post!'
                                                          ),
                               follow_redirects=True)

    assert response.status_code == 200
    assert len(Post.query.all()) == 2
    assert 'new post!' in response.data

def test_recip_add_post(test_client, test_full_database):
    response = test_client.post('/login',
                                data=dict(username='recip_user', password='recip', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data
    assert len(Post.query.all()) == 2

    response = test_client.get('/_create_post', query_string=dict(disaster_title='new post!',
                                                          disaster_description='body for new post!'
                                                          ),
                               follow_redirects=True)

    assert response.status_code == 200
    assert len(Post.query.all()) == 3
    assert 'new post!' in response.data

def test_invalid_description_add_post(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='recip_user', password='recip', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data

    assert len(Post.query.all()) == 2

    response = test_client.get('/_create_post', query_string=dict(disaster_title='new donor post !',
                                                                  disaster_description=''
                                                                  ),
                               follow_redirects=True)

    assert response.status_code == 200
    assert len(Post.query.filter_by(body='').all()) == 0

def test_invalid_title_add_post(test_client, test_full_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data
    assert len(Post.query.all()) == 2

    response = test_client.get('/_create_post', query_string=dict(disaster_title='',
                                                                  disaster_description='new donor post !'
                                                                  ),
                               follow_redirects=True)

    assert response.status_code == 200
    assert len(Post.query.filter_by(title='').all()) == 0

def test_invalid_both_add_post(test_client, test_full_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data
    assert len(Post.query.all()) == 2

    response = test_client.get('/_create_post', query_string=dict(disaster_title='',
                                                                  disaster_description=''
                                                                  ),
                               follow_redirects=True)

    assert response.status_code == 200
    assert len(Post.query.filter_by(title='').all()) == 0
