from app.models import *
from flask_login import current_user

def test_donor_donate_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data
    assert 'Admin Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    user = User.query.filter_by(user_type='D').first()
    user = user.id

    response = test_client.post('/_donate_item',
                               query_string=dict(item_id=1,
                                                 quantity=5,
                                                 donor_id=user),
                               follow_redirects=True)
    assert response.status_code == 200
    assert Item.query.first().quantity == 5
    assert len(Donate.query.all()) == 1

def test_admin_valid_donate_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data
    assert 'Admin Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    user = User.query.first().id

    response = test_client.post('/_donate_item',
                               query_string=dict(item_id=1,
                                                 quantity=5,
                                                 donor_id=user),
                               follow_redirects=True)
    assert response.status_code == 200
    assert Item.query.first().quantity == 5
    assert len(Donate.query.all()) == 1

def test_admin_recip_donate_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='recip_user', password='recip', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    user = User.query.filter_by(username='recip_user').first().id

    response = test_client.post('/_donate_item',
                               query_string=dict(item_id=1,
                                                 quantity=5,
                                                 donor_id=user),
                               follow_redirects=True)
    assert response.status_code == 200
    assert Item.query.first().quantity == 10
    assert len(Donate.query.all()) == 0
