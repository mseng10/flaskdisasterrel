from jinja2 import UndefinedError
from app.models import Post
from flask_login import current_user


def test_admin_post(test_client,test_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data

    response = test_client.get('/post1',follow_redirects=True)
    assert response.status_code == 200
    assert 'Admin Post' in response.data
    assert 'This is a test post for a admin user.' in response.data

def test_recip_post(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='recip_user', password='recip', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'Recip Post' in response.data
    assert 'This is a test post for recip user.' in response.data

def test_donor_post(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'Recip Post' in response.data
    assert 'This is a test post for recip user.' in response.data

def test_recep_null_post(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data

    try:
        response = test_client.get('/post3', follow_redirects=True)
        assert False
    except AttributeError:
        assert True


