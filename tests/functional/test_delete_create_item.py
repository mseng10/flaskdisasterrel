from sqlalchemy.orm.exc import UnmappedInstanceError
from app.models import Item



def test_valid_recip_delete_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='recip_user', password='recip', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200

    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_delete_item', query_string=dict(item_id=item.id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert len(Item.query.all()) == 0

def test_valid_admin_delete_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_delete_item', query_string=dict(item_id=item.id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert 'test item' not in response.data

    assert len(Item.query.all()) == 0

def test_invalid_donor_delete_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_delete_item', query_string=dict(item_id=item.id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert 'test item' not in response.data

    assert len(Item.query.all()) == 1


def test_invalid_delete_null_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code ==200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert len(Item.query.all()) == 1
    item = Item.query.first().id
    assert item == 1

    try:
        response = test_client.get('/_delete_item', query_string=dict(item_id=2),
                               follow_redirects=True)
        assert False
    except UnmappedInstanceError:
        assert True


def test_admin_add_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_create_item',
                               query_string=dict(item_name='new item',
                                                 item_quantity=5,
                                                 item_unit='volts',
                                                 post_id=item.post_id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert 'new item' in response.data
    assert len(Item.query.all()) == 2

def test_recip_add_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='recip_user', password='recip', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_create_item',
                               query_string=dict(item_name='new item',
                                                 item_quantity=5,
                                                 item_unit='volts',
                                                 post_id=item.post_id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert 'new item' in response.data
    assert len(Item.query.all()) == 2

def test_donor_invalid_add_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data
    assert 'Admin Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_create_item',
                               query_string=dict(item_name='new item',
                                                 item_quantity=5,
                                                 item_unit='volts',
                                                 post_id=item.post_id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert 'new item' in response.data
    assert len(Item.query.all()) == 1

def test_invalid_name_add_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data
    assert 'Admin Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_create_item',
                               query_string=dict(item_name='',
                                                 item_quantity=5,
                                                 item_unit='volts',
                                                 post_id=item.post_id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert len(Item.query.filter_by(name='').all()) == 0
    assert len(Item.query.all()) == 1

def test_invalid_name_add_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data
    assert 'Admin Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_create_item',
                               query_string=dict(item_name='new item',
                                                 item_quantity=-2,
                                                 item_unit='volts',
                                                 post_id=item.post_id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert len(Item.query.filter_by(quantity=-2).all()) == 0
    assert len(Item.query.all()) == 1

def test_invalid_units_add_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data
    assert 'Admin Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_create_item',
                               query_string=dict(item_name='new item',
                                                 item_quantity=5,
                                                 item_unit='',
                                                 post_id=item.post_id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert len(Item.query.filter_by(unit='').all()) == 0
    assert len(Item.query.all()) == 1

def test_invalid_broken_add_item(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Recip Post' in response.data
    assert 'Admin Post' in response.data

    response = test_client.get('/post2', follow_redirects=True)
    assert response.status_code == 200
    assert 'This is a test post for recip user.' in response.data
    assert 'recip item' in response.data

    assert len(Item.query.all()) == 1
    item = Item.query.first()
    assert item.post_id == 2

    response = test_client.get('/_create_item',
                               query_string=dict(item_name='',
                                                 item_quantity=0,
                                                 item_unit='',
                                                 post_id=item.post_id),
                               follow_redirects=True)
    assert response.status_code == 200
    assert len(Item.query.filter_by(name='').all()) == 0
    assert len(Item.query.filter_by(quantity=0).all()) == 0
    assert len(Item.query.filter_by(unit='').all()) == 0
    assert len(Item.query.all()) == 1