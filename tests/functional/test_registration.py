from app.models import User

def test_valid_recip_registration(test_client,test_database):
    assert len(User.query.all()) == 1

    response = test_client.get('/register', follow_redirects=True)

    assert response.status_code == 200
    response = test_client.post('/register',
                                data=dict(username='newuser',
                                          email='testuser@gmail.com',
                                          password='testuser',
                                          password2='testuser',
                                          user_type='R'
                                          ),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Congratulations, you are now a registered user!' in response.data

    assert len(User.query.filter_by(user_type='R').all()) == 1
    response = test_client.post('/login',
                                data=dict(username='newuser', password='testuser', remember_me=False),
                                follow_redirects=True)

    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data

def test_valid_donor_registration(test_client,test_database):
    assert len(User.query.all()) == 1

    response = test_client.get('/register', follow_redirects=True)

    assert response.status_code == 200
    response = test_client.post('/register',
                                data=dict(username='newuser',
                                          email='testuser@gmail.com',
                                          password='testuser',
                                          password2='testuser',
                                          user_type='D'
                                          ),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Congratulations, you are now a registered user!' in response.data

    assert len(User.query.filter_by(user_type='D').all()) == 1
    response = test_client.post('/login',
                                data=dict(username='newuser', password='testuser', remember_me=False),
                                follow_redirects=True)

    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data


def test_invalid_username_registration(test_client,test_database):
    response = test_client.post('/register',
                                data=dict(username='admin_user',
                                          email='testuser@gmail.com',
                                          password='testuser',
                                          password2='testuser',
                                          user_type='D'),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Please use a different username.' in response.data

    user = User.query.all()
    assert len(user) == 1

def test_invalid_email_registration(test_client,test_database):

    response = test_client.post('/register',
                                data=dict(username='new_user',
                                          email='admin@uiowa.edu',
                                          password='testuser',
                                          password2='testuser',
                                          user_type='D'),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Please use a different email address.' in response.data

    user = User.query.all()
    assert len(user) == 1

def test_invalid_password_registration(test_client,test_database):

    response = test_client.post('/register',
                                data=dict(username='new_user',
                                          email='new_user@uiowa.edu',
                                          password='testuser',
                                          password2='tesuser',
                                          user_type='D'),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Field must be equal to password.' in response.data

    user = User.query.all()
    assert len(user) == 1