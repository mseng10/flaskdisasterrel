from app.models import Item

def test_admin_search_posts(test_client,test_full_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data

    response = test_client.get('/admin',query_string=dict(search='admin',filter_by='Posts'),
                               follow_redirects=True)

    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data

def test_admin_search_items(test_client, test_full_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data

    response = test_client.post('/admin', query_string=dict(search='admin', filter_by='Items'),
                               follow_redirects=True)

    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data

def test_admin_search_donations(test_client, test_full_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data

    response = test_client.post('/admin', query_string=dict(search='admin', filter_by='Donations'),
                               follow_redirects=True)

    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
