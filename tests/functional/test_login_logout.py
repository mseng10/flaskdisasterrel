from app.models import User

def test_invalid_index(test_client):
    response = test_client.get('/index')
    assert response.status_code == 302

    response = test_client.get('/')
    assert response.status_code == 302

    response = test_client.post('/index')
    assert response.status_code == 302

def test_admin_login_logout(test_client,test_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Create post' in response.data
    assert 'Admin Post' in response.data
    assert 'Delete' in response.data

    response = test_client.get('/admin')
    assert response.status_code == 200

    response = test_client.get('/logout',
                                follow_redirects=True)
    assert response.status_code == 200


def test_donor_login_logout(test_client, test_full_database):
    response = test_client.post('/login',
                                data=dict(username='donor_user', password='donor', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Create post' not in response.data
    assert 'Admin Post' in response.data
    assert 'Recip Post' in response.data
    assert 'Delete' not in response.data

    response = test_client.get('/index')
    assert response.status_code == 200

    response = test_client.get('/logout',
                               follow_redirects=True)
    assert response.status_code == 200


def test_recip_login_logout(test_client, test_full_database):
    response = test_client.post('/login',
                                data=dict(username='recip_user', password='recip', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Disaster Management System - Tasks' in response.data
    assert 'Create post' in response.data
    assert 'Recip Post' in response.data
    assert 'Delete' in response.data

    response = test_client.get('/index')
    assert response.status_code == 200

    response = test_client.get('/logout',
                               follow_redirects=True)
    assert response.status_code == 200

def test_invalid_logout(test_client, test_database):
    response = test_client.post('/logout',
                               follow_redirects=True)
    assert response.status_code == 405

def test_invalid_username_login(test_client, test_database):
    response = test_client.post('/login',
                                data=dict(username='user', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Invalid username or password' in response.data

    response = test_client.get('/index')
    assert response.status_code == 302


def test_null_username_login(test_client, test_database):

    response = test_client.post('/login',
                                data=dict(username='', password='admin', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200

    response = test_client.get('/index')
    assert response.status_code == 302

def test_invalid_password_login(test_client, test_database):
    response = test_client.post('/login',
                                data=dict(username='', password='', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200

    response = test_client.get('/index')
    assert response.status_code == 302

def test_null_password_login(test_client, test_database):
    response = test_client.post('/login',
                                data=dict(username='admin_user', password='', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200

    response = test_client.get('/index')
    assert response.status_code == 302

def test_null_login(test_client, test_database):
    response = test_client.post('/login',
                                data=dict(username='', password='', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200

    response = test_client.get('/index')
    assert response.status_code == 302

def test_invalid_login(test_client, test_database):
    response = test_client.post('/login',
                                data=dict(username='user', password='ad', remember_me=False),
                                follow_redirects=True)
    assert response.status_code == 200
    assert 'Invalid username or password' in response.data

    response = test_client.get('/index')
    assert response.status_code == 302
