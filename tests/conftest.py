import os
import pytest
from app import create_app, db
from app.models import User, Post, Item, Donate

basedir = os.path.abspath(os.path.dirname(__file__))
TESTDB_PATH = os.path.join(basedir, 'test.db')

"""TestConfig class used to config testing db"""
class TestConfig(object):
    SECRET_KEY = 'team_2_disaster_rel_test'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + TESTDB_PATH
    TESTING = True
    DEBUG = True

    WTF_CSRF_ENABLED = False


"""null user object"""
@pytest.fixture(scope='module')
def null_user():
    return User()


"""donor user object"""
@pytest.fixture()
def donor_user():
    user = User(username='donor_user', email='donor@uiowa.edu', user_type='D')
    user.set_password('donor')
    return user


"""admin user object"""
@pytest.fixture()
def admin_user():
    user =User(username='admin_user', email='admin@uiowa.edu', user_type='admin')
    user.set_password('admin')
    return user


"""recip user object"""
@pytest.fixture()
def recip_user():
    user = User(username='recip_user', email='recip@uiowa.edu', user_type='R')
    user.set_password('recip')
    return user


"""null post object"""
@pytest.fixture()
def null_post():
    return Post()


"""new post object"""
@pytest.fixture()
def admin_post():
    return Post(title='Admin Post',body='This is a test post for a admin user.')

""""""
@pytest.fixture()
def recip_post():
    return Post(title='Recip Post',body='This is a test post for recip user.')

#TODO
@pytest.fixture()
def admin_item():
    return None

"""Donation object by donor"""
@pytest.fixture()
def donor_donation():
    return Donate(quantity=8)

"""Item created by recipient user"""
@pytest.fixture()
def recip_item():
    return Item(name='recip item', quantity=10, unit = 'units')

"""Test client used to simulate a flask application configured for testing."""
@pytest.fixture()
def test_client():
    flask_app = create_app(TestConfig)

    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()

    try:
        os.remove("test.db")
    except:
        pass

"""Database used for testing. Contains a single admin user."""
@pytest.fixture()
def test_database(admin_user, admin_post):

    db.create_all()
    admin_user.posts.append(admin_post)

    db.session.add(admin_user)
    db.session.add(admin_post)
    db.session.commit()

    yield db

    db.drop_all()
    os.unlink(TESTDB_PATH)

"""Database additions of recip users, donor users, items and donations"""
@pytest.fixture()
def test_full_database(test_database, donor_user, recip_user, recip_post, recip_item, donor_donation):

    recip_user.posts.append(recip_post)
    recip_post.Items.append(recip_item)

    test_database.session.add(donor_user)
    test_database.session.add(recip_user)
    test_database.session.add(recip_post)
    test_database.session.add(recip_item)

    #TODO
    #test_database.session.add(donor_donation)

    test_database.session.commit()

    yield test_database

    db.drop_all()
    os.unlink(TESTDB_PATH)

