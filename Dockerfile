FROM alpine:3.5

RUN apk add --no-cache openssl wget git curl python2 py2-pip gcc python2-dev musl-dev
RUN apk add --no-cache docker screen
RUN apk add --no-cache openssh-client

CMD ["/bin/sh"]
